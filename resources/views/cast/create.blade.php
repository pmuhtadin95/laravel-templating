@extends('layouts.master')

@section('content')
<div class="ml-4 mt-3 mr-4">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/cast" method="POST">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan nama">
                    <!-- @error('nama')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror -->
                  </div>
                  <div class="form-group">
                    <label for="age">Umur</label>
                    <input type="text" class="form-control" id="age" name="age" placeholder="Masukkan umur">
                    <!-- @error('umur')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror -->
                  </div>
                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea class="form-control" rows="3" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Masukkan bio"></textarea>
                    <!-- @error('bio')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror -->
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>

    
@endsection