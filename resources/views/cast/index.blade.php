@extends('layouts.master')

@section('content')
    <div class="ml-3 mr-3 mt-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Name</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse ($cast as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->nama}}</td>
                            <td>{{$value->umur}}</td>
                            <td>{{$value->bio}}</td>
                            <td>
                              <div class="btn-group">
                                <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <form action="/cast/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>
                              </div>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse  
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
    </div>
    
@endsection